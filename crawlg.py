from google_play_scraper import Sort, reviews
import json
import sys

job_id = '1'
username = 'a'
password = 'b'
#getreview
def getResult():
    result = reviews(
        'ovo.id',
        lang='id',
        country='id',
        count=99999999,
        sort=Sort.NEWEST
    )
    return result

#save result to json
def saveJson():
    result = getResult()
    with open('result_{0}.json'.format(job_id),'w') as f:
        f.write(json.dumps(result, indent=4, sort_keys=True, default=str))

if __name__ == '__main__':
    getResult()
    saveJson()