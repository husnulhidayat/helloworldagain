from bs4 import BeautifulSoup as bs
import requests
import pandas as pd
import sys
import json

def getData():
    for id in range(1, 10):
        url = 'https://itunes.apple.com/id/rss/customerreviews/page={}/id=1142114207/sortby=mostrecent/xml?urlDesc=/customerreviews/id=840784742/sortby=mostrecent/lxml'.format(
            id)
        page = requests.get(url)

        source = bs(page.text, 'lxml')
        results = source.find_all('entry')
        
        ls=[]
        for result in results:
            idd = result.find('id').text
            name = result.find('name').text
            title = result.find('title').text
            content = result.find('content').text
            date = result.find('updated').text
            rating = result.find('im:rating').text
            version = result.find('im:version').text

            ls.append({
                'id': idd,
                'name': name,
                'title': title,
                'content': content,
                'date': date,
                'rating': rating,
                'version': version
            })
            

        gluster_d= 'raw.json'
        with open(gluster_d,'a') as f:
            f.write(json.dumps(ls, sort_keys=True, indent=4))
                
getData()
